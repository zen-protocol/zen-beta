# Zen Protocol Beta Documentation

## Installation Instructions

### Windows

1. Download and install [.net 4.5](http://www.microsoft.com/en-us/download/details.aspx?id=30653)
2. Download and install [Microsoft Visual C++](https://www.microsoft.com/en-us/download/details.aspx?id=48145)
3. Download and install [gtk2](https://sourceforge.net/projects/gtk-win/)
4. Download and install [gtk-sharp-2](https://dl.xamarin.com/GTKforWindows/Windows/gtk-sharp-2.12.44.msi)
5. Download and install the [Zen Client](https://gitlab.com/zen-protocol/zen-beta/raw/master/installers/windows/zen_staging_package.msi)

### Mac OSX

1. Install [Mono for Mac](https://download.mono-project.com/archive/5.0.1/macos-10-universal/MonoFramework-MDK-5.0.1.1.macos10.xamarin.universal.pkg)
2. Download and install [Zen](https://gitlab.com/zen-protocol/zen-beta/raw/master/installers/mac/Zen.dmg)
3. NOTE: when opening the app for the first time, right click the icon of the app and then click open. You will then receive a notification along the lines of "Zen can't be opened because it is from an unidentified developer." - click the "Open" button.

If you would like to run the wallet with an active miner do the following:
* `cd /Applications/Zen.app/Contents/Resources/`
* `mono zen.exe -m`

### Linux

1. Install [Mono for Linux](http://www.mono-project.com/download/#download-lin)
  * `sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF`
  * `echo "deb http://download.mono-project.com/repo/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/mono-official.list`
  * `sudo apt-get update`
  * `sudo apt-get install mono-devel`
2. Install [fsharp](http://fsharp.org/use/linux/)
3. Install libsodium-dev.
  * For Debian / Ubuntu run `sudo apt install libsodium-dev`
4. Install GTK `sudo apt install gtk2.0`
5. Run `sudo apt-get update`
6. Download and extract [zen.tar.gz](https://gitlab.com/zen-protocol/zen-beta/raw/master/installers/linux/zen.tar.gz)
7. Open shell and navigate to your extracted zen folder
8. Run `mono zen.exe`

If you would like to run the wallet with an active miner do the following:
* `mono zen.exe -m`
* When your miner finds a block you will receive 1,000 Kalapas (the smallest unit of the ZEN token) (which will be visible in your Balance tab)

## Using the Beta

The Beta is a combination of using [demo.zenprotocol.com](http://demo.zenprotocol.com) and the desktop wallet.
* [demo.zenprotocol.com](http://demo.zenprotocol.com) includes the following:
  * [Contract explorer](http://demo.zenprotocol.com) - for browsing existing contracts
  * [Contract templates](http://demo.zenprotocol.com/ContractCreation) - for deploying your own
  * [Faucet](http://demo.zenprotocol.com/Faucet) - to get your hands on some Zen
  * [An oracle](http://demo.zenprotocol.com/Oracle) - We have prepared for the use of the demo
* The desktop wallet - is a full node, that connects to the Zen testnet and allows you to send, receive and sign transactions, and activate smart contracts.

For simplification we will use a short hand of (Product/tab) to help you understand which product and which tab to go to.

For example:
(Wallet/Portfolio) - refers to the desktop wallet app in the 'Portfolio' tab
(Web/Faucet) - is referring to the demo.zenprotocol.com website in the 'Faucet' page

### Usage Instructions

1. Get ZEN tokens
  * (Wallet/Wallet) Copy your address
  * ([Web/Faucet](http://demo.zenprotocol.com/Faucet) - paste your address and click submit
  * (Wallet/Portfolio) You should now have 0.5 ZEN
  * (Wallet) In the footer of the wallet check that your client has finished syncing and downloaded the tip of the block chain
2. Create a Secure Token (This is needed for issuing a call option contract)
  * Create the contract
    * (Web) Go to the [Secure Token contract template page](http://demo.zenprotocol.com/ContractCreation/FromTemplate/TokenGenerator)
    * (Web) Give the token a name (i.e. - Johns secure token)
    * (Wallet) Copy the address from your Zen wallet
    * (Web) Paste the address and click "Submit"
    * (Web) On the next screen - copy the contract code    
  * (Wallet/Contract) Activate the contract
    * In your Zen wallet - go to the "Contract" tab
    * Paste the contract code on the left pane
    * Click the "Create/Renew" button to activate the contract
    * use the up arrow to activate the contract for 10 blocks
    * Don't choose any options from the Select secure token dropdown
    * Click "Apply"
    * ([Web/Explorer](http://demo.zenprotocol.com)) In the 'Active Until' column - you should see your contract go from 'inactive' - to show a number - meaning it is Active
  * Get some Secure tokens
    * Once the contract is active
    * Click on the address of the contract
    * Click on the 'Get Tokens' button
    * Click 'Submit' again on the next page
    * (Wallet/Wallet) Sign a new transaction in your Zen wallet
      * (Wallet/Wallet) Go to the 'Wallet' tab
      * (Wallet/Wallet) Click the 'Send' button
      * (Web) Copy the 'address' field
      * (Wallet) Paste in the destination field
      * (Web) Copy the 'Data' field
      * (Wallet) Paste in the 'Data' field
      * (Wallet) No need to 'Select asset'
      * (Wallet) Enter 1 in the 'Amount to send' field
      * (Wallet) Click 'Sign and Review'
      * (Wallet) Click 'Transmit' (once)
      * (Wallet/Portfolio) Go to the 'Portfolio' tab - you should now see 1000 units of your secure token asset
3. Create a call option
  * (Web/Templates) - Click 'Create' on the [Vanilla Call Option](http://demo.zenprotocol.com/ContractCreation/FromTemplate/CallOption) Template
  * (Web) Fill in the fields and click 'Create'
    * Name - give it any name you want
    * Numeraire - is currently hard coded to the ZEN token address
    * Control Asset - choose the Secure Token you created
    * Oracle - Is hard coded to a the Zen oracle created for the demo
    * Control Asset Return - copy and paste your wallet address
    * Underlying - choose the stock ticker you want to issue call options for
      * Choose from the tickers our [oracle](http://demo.zenprotocol.com/Oracle) is creating commitments to
    * Premium - is the amount of Kalapas (the smallest unit of ZEN) required to purchase one call option
    * Strike - is the price at which the buyer of a call option can exercise the option later (make the strike lower than the current price of the ticker in order to realize a profit)
    * Owner PubKey - (Wallet/Wallet) - click the 'Copy Public Key' button and paste it
    * Click 'Create'
  * (Web) Copy the contract code
  * (Wallet/Contract) Activate the contract
    * Paste the contract code in the left pane
    * Click 'Create/Renew' button
    * Choose the number of blocks you want to activate the contract for (50 blocks recommended)
    * In the 'Select secure token' field - choose the secure token you created previously
    * Click the 'Button' button
    * (Web/Explorer) wait until you see that your new contract is active
4. Collateralize the call option
  * (Web) Click on your newly created contract
  * (Web) Click on the 'Collateralize' button
  * (Wallet) Copy your ZEN address
  * (Web) Paste in the 'Return Address' field
  * (Web/Transaction Details) Copy the 'Address' field
  * (Wallet/Wallet) Click on the 'Send' button
  * (Wallet) Paste your address in the address field
  * (Web/Transaction Details) Copy the 'Data' field
  * (Wallet) Paste your data in the data field
  * (Wallet) Select asset - choose 'Zen'
  * (Wallet) Amount to send: enter 1,000,000 - if your premium was 50 - then the contract will be able to issue 20,000 options
  * (Wallet) Click Sign & Review
  * (Wallet) Click 'Transmit'
  * (Web) You should be able to know that the collateralize action went through by opening the contract page and seeing that the UTXO field says - 2
5. Buy some call options
  * (Web) Go to your call option page
  * (Web) Click on the 'Buy' button
  * (Wallet) Copy your address
  * (Web) Paste your address in the 'Send to public key address' field and click 'Submit'
  * (Web/Transaction Details) Copy the 'Address' field
  * (Wallet/Wallet) Click on the 'Send' button
  * (Wallet/Send) Paste your address in the address field
  * (Web/Transaction Details) Copy the 'Data' field
  * (Wallet/Send) Paste your data in the data field
  * (Wallet/Send) Select asset - choose 'Zen'
  * (Wallet/Send) Amount to send - if the premium is 50 - then enter an amount devisable by 50 - the contract then will issue you option tokens representing your claim for the collateral in the contract in the case that the price of the ticker is above the strike price
  * (Wallet/Send) Click the 'Sign & Review' button
  * (Wallet/Sign & Review) Click 'Transmit'
  * (Wallet/Portfolio) You should now see that you have the call option tokens
6. Exercise the option
  * (Web) Go to the call option page
  * (Web) Click the 'Exercise' button
  * (Wallet/Wallet) Copy your wallet address
  * (Web) Paste your address in the 'Return address' field and click 'Submit'
  * (Web/Transaction Details) Copy the 'Address' field
  * (Wallet/Wallet) Click on the 'Send' button
  * (Wallet/Send) Paste your address in the address field
  * (Web/Transaction Details) Copy the 'Data' field
  * (Wallet/Send) Paste your data in the data field
  * (Wallet/Send) Select asset - choose the call option token - since you are now sending it back to the contract in order to exercise it
  * (Wallet/Send) Amount to send - choose how many options you want to exercise
  * (Wallet/Send) Click the 'Sign & Review' button
  * (Wallet/Sign & Review) Click 'Transmit'
  * (Wallet/Portfolio) You should now see you have less call option tokens
  * (Wallet/Balance) You should see a transaction for sending your call option tokens
  * (Wallet/Balance) Choose the 'Zen' token in the 'Asset' dropdown - you should see a new incoming transaction of your profit from exercising the option.

Please contact info@zenprotocol.com if you have any issues
