# Zen protocol 测试版文档

## 安装指南

### Mac OSX

1. 安装 [Mono for Mac](https://download.mono-project.com/archive/5.0.1/macos-10-universal/MonoFramework-MDK-5.0.1.1.macos10.xamarin.universal.pkg)
2. 下载并安装 [Zen](https://gitlab.com/zen-protocol/zen-beta/raw/master/installers/mac/Zen.dmg)
3. 注意：当第一次打开Zen应用时，右键单击应用图标然后点击“open”，之后会弹出一个通知内容是“Zen can't be opened because it is from an unidentified developer.”，点击“Open”按钮。

### Linux

1. 安装 [fsharp](http://fsharp.org/use/linux/)
2. 安装 libsodium-dev.
  * 对于 Debian / Ubuntu，运行 `sudo apt install libsodium-dev`
3. 安装 [Mono for Linux](http://www.mono-project.com/download/#download-lin)
4. 安装 GTK `sudo apt install gtk2.0`
5. 运行 `sudo apt-get update`
6. 下载并解压缩 [zen.tar.gz](https://gitlab.com/zen-protocol/zen-beta/raw/master/installers/linux/zen.tar.gz)
7. 打开shell并前往解压后的zen文件夹
8. 运行 `mono zen.exe`


## 使用测试版应用

测试版是由[demo.zenprotocol.com](http://demo.zenprotocol.com) 和桌面钱包功能组合而成的.
* [demo.zenprotocol.com](http://demo.zenprotocol.com) 包含下面的内容:
  * [合约浏览器(Contract explorer)](http://demo.zenprotocol.com) - 为了浏览已经存在的合约
  * [合约模板(Contract templates)](http://demo.zenprotocol.com/ContractCreation) - 为了部署你自己的合约
  * ["水龙头"(Faucet)](http://demo.zenprotocol.com/Faucet) - 用来获取一些测试的Zen
  * [预测市场(An oracle)](http://demo.zenprotocol.com/Oracle) - 为了使用demo，我们准备了一些
* 桌面钱包是一个全功能节点，可以连接Zen的测试网络，允许你对交易进行发送、接收、签名，并且激活智能合约。

为了简单起见，我们使用缩写(应用/标签)(Product/tab)来帮助你理解应该转到哪一个应用和标签。

例如For example:  
(Wallet/Portfolio) - 指示的是桌面钱包应用的‘Portfolio’标签  
(Web/Faucet) - 指示的是demo.zenprotocol.com网页上的‘Faucet’页面。

### 使用指南

1. 获取Zen代币
  * (Wallet/Wallet) 复制你的地址
  * ([Web/Faucet](http://demo.zenprotocol.com/Faucet) - 粘贴你的地址并点击submit
  * (Wallet/Portfolio) 你应该可以看到有0.5个Zen
  * (Wallet) 在钱包的页脚检查你的客户端是否完成同步并下载了区块链中的未确认的交易(tip)
2. 创建一个安全令牌(用来发行看涨期权(call option)合约)
  * 创建合约Create the contract
    * (Web) 转到[安全令牌合约模板页面](http://demo.zenprotocol.com/ContractCreation/FromTemplate/TokenGenerator)
    * (Web) 给令牌起一个名字(如 - Zen安全令牌)
    * (Wallet) 从Zen钱包复制你的地址
    * (Web) 粘贴地址并点击“submit”
    * (Web) 在接下来的页面中复制合约代码(contract code)
  * (Wallet/Contract) 激活合约
    * 在你的Zen钱包中，转到“Contract”标签
    * 在左侧窗格内粘贴合约代码
    * 点击"Create/Renew"按钮来激活合约
    * 使用向上箭头选择10来激活合约，连续10个区块[_check_]
    * 不要从“Select secure token”下拉框中选择任何选项
    * 点击 "Apply"
    * ([Web/Explorer](http://demo.zenprotocol.com)) 在‘Active Until’一列，你应当可以看到自己的合约从‘inactive’状态到显示一个数字，意味着合约已经激活了
  * 获取一些安全令牌
    * 一旦合约有效
    * 点击合约的地址
    * 点击“Get Tokens”按钮
    * 点击下一个页面的 'Submit'
    * (Wallet/Wallet) 在Zen钱包中签名一个新的交易
      * (Wallet/Wallet) 转到'Wallet'标签
      * (Wallet/Wallet) 点击'Send'按钮
      * (Web) 复制'address'一栏的值
      * (Wallet) 粘贴进‘Destination’栏
      * (Web) 复制'Data'一栏的值
      * (Wallet) 粘贴进'Data'栏
      * (Wallet) 不需要'Select asset'
      * (Wallet) 在'Amount to send'一栏中输入1 
      * (Wallet) 点击'Sign and Review'
      * (Wallet) 点击'Transmit' (只点击一次)
      * (Wallet/Portfolio) 转到'Portfolio'标签 - 你现在应该可以看见1000个自己的安全令牌资产
3. 创建一个看涨期权
  * (Web/Templates) - 点击'Create'，在[Vanilla Call Option](http://demo.zenprotocol.com/ContractCreation/FromTemplate/CallOption) 模板上面
  * (Web) 填写完这些栏，然后点击'Create'
    * Name - 给它起一个你想要的名字
    * Numeraire(货币兑换率计价标准) - 当前为硬编码的Zen代币地址
    * Control Asset - 选择你创建的安全令牌
    * Oracle(预测市场) - 当前为硬编码的Zen预测市场，用以演示
    * Control Asset Return - 复制和粘贴你的钱包地址
    * Underlying(标的物) - 选择发行看涨期权的股票行情自动收录器
      * 从我们的[oracle](http://demo.zenprotocol.com/Oracle)所委托的行情自动收录器中选择
    * Premium(溢价) - 是购买一个看涨期权所需的Kalapas(Zen代币的最小单位)数量
    * Strike - 是一个看涨期权的购买者之后能够行权的价格(让strike价格低于行情自动收录器的当前价格，才能实现获利)
    * Owner PubKey - (Wallet/Wallet) - 点击'Copy Public Key'按钮并粘贴到这里
    * 点击 'Create'
  * (Web) 复制合约代码
  * (Wallet/Contract) 激活这一合约
    * 在左侧窗格中粘贴这个合约代码
    * 点击'Create/Renew'按钮
    * 选择你想要激活合约的区块数量(建议50个区块)
    * 在'Select secure token'一栏选择你之前创建的安全令牌
    * 点击'Apply' 按钮
    * (Web/Explorer) 观察直到新合约变为有效的状态
4. 抵押看涨期权
  * (Web) 点击你刚刚创建的合约
  * (Web) 点击'Collateralize'按钮
  * (Wallet) 复制你的Zen钱包地址
  * (Web) 粘贴进'Return Address'一栏
  * (Web/Transaction Details) 复制'Address'栏的值
  * (Wallet/Wallet) 点击'Send'按钮
  * (Wallet) 粘贴地址在“Destination”一栏
  * (Web/Transaction Details) 复制'Data'栏的值
  * (Wallet) 粘贴数据在“Data”一栏
  * (Wallet) Select asset - 选择'Zen'
  * (Wallet) Amount to send: 输入 1,000,000 - 如果你的溢价是50，那么合约可以发行20,000份期权
  * (Wallet) 点击 Sign & Review
  * (Wallet) 点击 'Transmit'
  * (Web) 打开合约页面，你应该可以看到抵押操作完成，并且UTXO一栏的值为2
5. 购买看涨期权
  * (Web) 转到看涨期权页面
  * (Web) 点击 'Buy' 按钮
  * (Wallet) 复制钱包地址
  * (Web) 粘贴地址到 'Send to public key address'一栏并点击'Submit'
  * (Web/Transaction Details) 复制'Address'栏的值
  * (Wallet/Wallet) 点击'Send'按钮
  * (Wallet/Send) 粘贴地址在‘Destination’栏
  * (Web/Transaction Details) 复制'Data'栏的值
  * (Wallet/Send) 粘贴数据到“Data”栏
  * (Wallet/Send) Select asset - 选择 'Zen'
  * (Wallet/Send) Amount to send - 如果溢价是50，那么输入可以被50整除的数字，之后合约会向你发行期权令牌，代表了对合约中抵押物的索赔权益，这是在行情自动收录器中的价格高于执行价格情况下。
  * (Wallet/Send) 点击'Sign & Review' 按钮
  * (Wallet/Sign & Review) 点击 'Transmit'
  * (Wallet/Portfolio) 现在应该能够看到看涨期权令牌
6. 行权
  * (Web) 转到看涨期权合约页面
  * (Web) 点击'Exercise' 按钮
  * (Wallet/Wallet) 复制钱包地址
  * (Web) 粘贴地址到'Return address'一栏并点击'Submit'
  * (Web/Transaction Details) 复制'Address'一栏的值
  * (Wallet/Wallet) 点击'Send'按钮
  * (Wallet/Send) 粘贴地址到'Destination'一栏
  * (Web/Transaction Details) 复制'Data'一栏的值
  * (Wallet/Send) 粘贴数据到‘Data’一栏
  * (Wallet/Send) Select asset - 选择对应的看涨期权令牌，因为现在你要把它发送回合约，从而实现行权
  * (Wallet/Send) Amount to send - 选择你想要行权的期权数量
  * (Wallet/Send) 点击'Sign & Review'按钮
  * (Wallet/Sign & Review) 点击'Transmit'
  * (Wallet/Portfolio) 现在你应该注意到拥有的看涨期权令牌变少了
  * (Wallet/Balance) 你应当能够看到一个发送你的看涨期权令牌的交易
  * (Wallet/Balance) 在'Asset'下拉框选择'Zen'令牌，你应当能够看到一笔新的收入交易，它代表着你行权产生的盈利.

有任何问题，请联系：info@zenprotocol.com
